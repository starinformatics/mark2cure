from django.contrib import admin

from mark2cure.common.models import Task, DocumentQuestRelationship

admin.site.register(Task)
admin.site.register(DocumentQuestRelationship)
