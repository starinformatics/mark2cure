from django.contrib.auth.models import User
from django.test import TestCase, Client, LiveServerTestCase

from selenium.webdriver.firefox.webdriver import WebDriver


class SimpleTest(TestCase):
    def setUp(self):
        # Animal.objects.create(name="lion", sound="roar")
        # Animal.objects.create(name="cat", sound="meow")
        self.client = Client()

        user = User.objects.create_user('test-admin', 'test-admin@localhost.com', 'test-admin-password')
        user.is_staff = True
        user.save()

        self.client.login(username='fred', password='secret')

    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)


class MySeleniumTests(LiveServerTestCase):
    fixtures = ['user-data.json']

    @classmethod
    def setUpClass(cls):
        cls.selenium = WebDriver()
        super(MySeleniumTests, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(MySeleniumTests, cls).tearDownClass()

    def test_login(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/login/'))

        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('myuser')

        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('secret')

        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()

