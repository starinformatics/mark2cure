from django.contrib import admin

from mark2cure.account.models import UserProfile, Ncbo

admin.site.register(UserProfile)
admin.site.register(Ncbo)
