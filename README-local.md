Adding design documents and discussion in [Google Drive doc](https://docs.google.com/document/d/1r2SP4MRwPa_gztIy-tP5GieyjKs6cnImfruM1-6eAHI/edit)



# Mark2Cure

[Mark2Cure](http://mark2cure.org/) is a web application concept recognition, relationship extraction and validation tool to generate annotations in fun and exploratory way to help scientific research!

Scientific communication is broken.

Progress in biomedical science is all about incrementally building off of the work of others. Currently, it takes far too long for one scientist's work to reach all the other scientists that would benefit. Mark2Cure will make scientific communication more efficient for everyone, leading to faster discoveries and cures for disease.

To be successful, we need your help. Mark2Cure works by directly involving crowds of people just like you.

## Setup

Note: '<user>' is your user account name on the system...

### Server Dependencies

* `sudo apt-get update`
* `sudo apt-get upgrade`
* `sudo apt-get install build-essential python python-dev python-pip python-virtualenv libmysqlclient-dev git-core nginx supervisor rabbitmq-server graphviz libgraphviz-dev pkg-config libncurses5-dev`

### Dependencies for Python Django tests(?)

# strange to install firefox on a server where 
# it cannot be directly used by users, but...
# it's a Selenium dependency! 
# Oops! Needs X-windows to work... alas...problematic 
# in a Linux server-only environment... 
#
# Ubuntu desktop can be installed for this? i.e.
* `sudo apt-get install ubuntu-desktop`
* `sudo apt-get install selenium firefox`

### Project Setup

* Make the project folder and download the repo to a suitable location (suggesting /opt here)
* `sudo mkdir -p /home/deploy/webapps; sudo -R chown <user> /home/deploy`
* `cd /home/deploy/webapps && git clone https://USER@bitbucket.org/sulab/mark2cure.git && cd mark2cure`

* Make the python virtual environment and activate it
* `cd /opt; sudo virtualenv mark2cure-venv'
* `sudo chown -R <user>:<user> mark2cure-venv`
* `. /opt/mark2cure-venv/bin/activate`

* Install all the python related dependencies
* `easy_install -U distribute`
* `pip install -r requirements.txt`

### Django Configuration (to allow manage.py to work...)
* The "settings.py_template" file in the mark2cure source code folder needs to be renamed to settings.py
* The template file has various TODO items (noted in comments) that require filling in, mostly credentials for resource access.

### Database Migrations
* `cd /home/deploy/webapps`
* `/opt/mark2cure-venv/bin/python manage.py schemamigration mark2cure --auto CHANGE_MESSAGE`
* `/opt/mark2cure-venv/bin/python manage.py migrate mark2cure`

### Control

* `. /opt/mark2cure-venv/bin/activate`
* `cd webapps/mark2cure/ && git pull origin HEAD`
* `sudo supervisorctl restart mark2cure`

* `python /opt/python/current/app/manage.py celeryd -v 2 -E -l INFO`
* `python /opt/python/current/app/manage.py celerybeat`

### Utils

* Flow diagram of the database relationships
* `python manage.py graph_models -a -o myapp_models.png`

### Supervisor
* Copy over scripts/supervisor/mark2cure.conf-production or local to /etc/supervisor/conf.d/mark2cure.conf

### NGINX
* Copy over scripts/nginx/sites-available/default-production to /etc/nginx/site-available/default

### GUNICORN

* Copy over scripts/gunicorn/start-production to /bin/gunicorn_start

